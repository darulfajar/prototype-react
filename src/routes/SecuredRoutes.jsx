import {
  Switch,
  Route,
} from 'react-router-dom'
import LearningCenter from '@screens/secured/LearningCenter'
import Account from '@screens/secured/Account'
import AccountPassword from '@screens/secured/AccountPassword'

function SecuredRoutes() {
  return (
    <Switch>
      <Route exact path="/home/learning-center" component={LearningCenter} />
      <Route path="/akun/password" component={AccountPassword} />
      <Route path="/akun" component={Account} />
    </Switch>
  )
}

export default SecuredRoutes
