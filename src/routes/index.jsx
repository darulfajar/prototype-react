import {
  Switch, Route, BrowserRouter, Redirect,
} from 'react-router-dom'
import { connect } from 'react-redux'
import { securedRoutePath } from '@lib/constant'
import Login from '@screens/auth/Login'
import LinkSent from '@screens/auth/LinkSent'
import SecuredRoutes from './SecuredRoutes'

function Routes(props) {
  const { isLoggedIn } = props

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Redirect to="/login" />
        </Route>
        <Route path="/login">
          {
            isLoggedIn ? <Redirect to={`${securedRoutePath}/learning-center`} /> : <Login />
          }
        </Route>
        <Route path="/link-sent" component={LinkSent} />
        <Route path={`${securedRoutePath}/learning-center`}>
          {
            !isLoggedIn ? <Redirect to="/login" /> : <SecuredRoutes />
          }
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

const mapState = ({ auth }) => ({ isLoggedIn: auth.isLoggedIn })
const connector = connect(mapState)
const ConnectedRouted = connector(Routes)
export default ConnectedRouted
