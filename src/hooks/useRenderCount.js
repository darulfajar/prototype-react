/* eslint-disable no-console */
import { isDev } from '@lib/env'
import { useRef, useEffect } from 'react'

export default function useRenderCount(name) {
  const counter = useRef(0)

  useEffect(() => {
    counter.current += 1
    if (isDev) console.log(`${name} rendering ${counter.current}`)
  })
}
