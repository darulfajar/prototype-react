/** @jsxImportSource @emotion/react */

import { Component } from 'react'
import Paper from '@material-ui/core/Paper'
import { css } from '@emotion/react'

const CSS = css`
  white-space: pre-wrap;
`

class ErrorBoundary extends Component {
  state = { error: null, errorInfo: null }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo,
    })
  }

  render() {
    const { errorInfo, error } = this.state
    const { children, title } = this.props
    if (errorInfo) {
      return (
        <Paper>
          {
            title
            && (
              <h2>{title}</h2>
            )
          }
          <details css={CSS}>
            {error && error.toString()}
            <br />
            {errorInfo.componentStack}
          </details>
        </Paper>
      )
    }
    return children
  }
}

export default ErrorBoundary
