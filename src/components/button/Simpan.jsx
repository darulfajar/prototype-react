/** @jsxImportSource @emotion/react */

import { css } from '@emotion/react'
import MuiButton from '@material-ui/core/Button'

const buttonCSS = css`
  &.MuiButton-root {
    border-radius: 5px;
    min-width: 120px;
    width: 120px;
    height: 45px;
    position: relative;
  }
  &.MuiButton-textPrimary {
    background-color: #556FB5;
  }
  .MuiButton-label {
    font-family: Poppins;
    font-size: 14px;
    font-style: normal;
    font-weight: 500;
    line-height: 100%;
    letter-spacing: 0em;
    text-align: center;
    position: absolute;
  }
`
function Button(props) {
  const { className, onClick: handleClick } = props
  return (
    <MuiButton
      disableElevation
      variant="contained"
      css={buttonCSS}
      className={className}
      onClick={handleClick}
    >
      Simpan
    </MuiButton>
  )
}

export default Button
