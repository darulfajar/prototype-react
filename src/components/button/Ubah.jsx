/** @jsxImportSource @emotion/react */

import { css } from '@emotion/react'
import IconButton from '@material-ui/core/IconButton'
import editSVG from '@assets/button/edit.svg'
import { openDialog } from '@lib/redux/slice/ui'
import { useLocation } from 'react-router-dom'
import { useDispatch } from 'react-redux'

const CSS = css`
  width: 30px;
  height: 30px;
`

const useOpenDialog = (type, data) => {
  const { pathname } = useLocation()
  const dispatch = useDispatch()
  return () => dispatch(openDialog({ type: `${pathname}.${type}`, data }))
}

function Button(props) {
  const { type, data } = props
  const handleClick = useOpenDialog(type, data)
  return (
    <IconButton css={CSS} size="small" onClick={handleClick}>
      <img src={editSVG} alt="" />
    </IconButton>
  )
}

export default Button
