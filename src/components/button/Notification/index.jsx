/* eslint-disable max-len */
/** @jsxImportSource @emotion/react */

import IconButton from '@material-ui/core/IconButton'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import Grow from '@material-ui/core/Grow'
import Popper from '@material-ui/core/Popper'
import Paper from '@material-ui/core/Paper'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import {
  Fragment, useEffect, useRef, useState,
} from 'react'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Typography from '@material-ui/core/Typography'
import { css } from '@emotion/react'

import notificationButtonSVG from './svg/notificationButton.svg'
import notificationAvatarSVG from './svg/notificationAvatar.svg'

const listCSS = css`
  &.MuiList-root {
    max-width: 341px;
  }
  & .MuiListItem-gutters {
    padding-right: 13px;
    padding-left: 13px;
  }
`

const testCSS = css`
  & > .MuiPaper-root {
    max-height: 100px;
    overflow-y: auto;
  }
`

function AccountButton() {
  const [open, setOpen] = useState(false)
  const anchorRef = useRef(null)

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen)
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }

    setOpen(false)
  }

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault()
      setOpen(false)
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = useRef(open)
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus()
    }

    prevOpen.current = open
  }, [open])

  return (
    <Fragment>
      <IconButton
        className="notification-button"
        ref={anchorRef}
        aria-controls={open ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        onClick={handleToggle}
      >
        <img src={notificationButtonSVG} alt="" />
      </IconButton>
      <Popper css={testCSS} open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList css={listCSS} autoFocusItem={open} onKeyDown={handleListKeyDown}>
                  <MenuItem alignItems="flex-start" onClick={handleClose}>
                    <ListItemAvatar>
                      <img src={notificationAvatarSVG} alt="" />
                    </ListItemAvatar>
                    <ListItemText
                      primary="Lorem ipsum dolor sit amet"
                      secondary={(
                        <Fragment>
                          <Typography
                            component="span"
                            variant="inherit"
                            noWrap
                            style={{ textOverflow: 'ellipsis' }}
                          >
                            Ill be in your neighborhood doing errands this
                          </Typography>
                        </Fragment>
                      )}
                    />
                  </MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </Fragment>
  )
}

export default AccountButton
