import { jsx } from 'slate-hyperscript'

const ELEMENT_TAGS = {
  BLOCKQUOTE: () => ({ type: 'quote' }),
  H1: () => ({ type: 'heading-one' }),
  H2: () => ({ type: 'heading-two' }),
  H3: () => ({ type: 'heading-three' }),
  H4: () => ({ type: 'heading-four' }),
  H5: () => ({ type: 'heading-five' }),
  H6: () => ({ type: 'heading-six' }),
  LI: () => ({ type: 'list-item' }),
  OL: () => ({ type: 'numbered-list' }),
  P: () => ({ type: 'paragraph' }),
  PRE: () => ({ type: 'code' }),
  UL: () => ({ type: 'bulleted-list' }),
}

const TEXT_TAGS = {
  DEL: () => ({ strikethrough: true }),
  EM: () => ({ italic: true }),
  STRIKE: () => ({ strikethrough: true }),
  STRONG: () => ({ bold: true }),
  U: () => ({ underline: true }),
}

const deserializer = (el) => {
  if (el.nodeType === 3) {
    return el.textContent
  } if (el.nodeType !== 1) {
    return null
  } if (el.nodeName === 'BR') {
    return '\n'
  }

  const { nodeName } = el
  let parent = el

  if (
    nodeName === 'PRE'
    && el.childNodes[0]
    && el.childNodes[0].nodeName === 'CODE'
  ) {
    ({ childNodes: [parent] } = el)
  }
  const children = Array.from(parent.childNodes)
    .map(deserializer)
    .flat()

  if (el.nodeName === 'BODY') {
    if (children) {
      console.log(children)
    }
    return jsx('fragment', {}, children)
  }

  if (ELEMENT_TAGS[nodeName]) {
    const attrs = ELEMENT_TAGS[nodeName](el)
    if (attrs.type === 'paragraph' && children.length < 1) {
      return '\n'
    }
    return jsx('element', attrs, children)
  }

  if (TEXT_TAGS[nodeName]) {
    const attrs = TEXT_TAGS[nodeName](el)
    return children.map((child) => jsx('text', attrs, child))
  }

  return children
}

export default (html) => {
  const parsed = new DOMParser().parseFromString(html, 'text/html').body
  console.log({ parsed })
  const deserialize = deserializer(parsed)
  return deserialize
}
