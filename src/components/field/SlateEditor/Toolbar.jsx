import MarkButton from './button/Mark'
import BlockButton from './button/Block'
import { Toolbar as BaseToolbar } from './components'

export default function Toolbar(props) {
  const { className } = props
  return (
    <BaseToolbar className={className}>
      <MarkButton format="bold" icon="B" />
      <MarkButton format="italic" icon="I" />
      <MarkButton format="underline" icon="U" />
      <MarkButton format="strikethrough" icon="S" />
      <BlockButton format="numbered-list" icon="OL" />
      <BlockButton format="bulleted-list" icon="UL" />
    </BaseToolbar>
  )
}
