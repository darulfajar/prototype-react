import escapeHtml from 'escape-html'
import { Text } from 'slate'

export default function serialize(nodes) {
  return nodes.reduce((prev, node) => {
    if (Text.isText(node)) {
      let text = escapeHtml(node.text)
      if (node.bold) text = `<strong>${text}</strong>`
      if (node.italic) text = `<em>${text}</em>`
      if (node.underline) text = `<u>${text}</u>`
      if (node.strikethrough) text = `<del>${text}</del>`
      return `${prev}${text}`
    }
    const children = serialize(node.children)
    if (node.type === 'paragraph') {
      return children ? (`${prev}<p>${children}</p>`) : (`${prev}<br />`)
    }
    if (node.type === 'numbered-list') {
      return (`${prev}<ul>${children}</ul>`)
    }
    if (node.type === 'bulleted-list') {
      return (`${prev}<ol>${children}</ol>`)
    }
    if (node.type === 'list-item') {
      return (`${prev}<li>${children}</li>`)
    }
    return children
  }, '')
}
