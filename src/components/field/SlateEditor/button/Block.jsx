import { useSlate } from 'slate-react'
import { Button, Icon } from '../components'
import { toggleBlock, isBlockActive } from './common'

export default function BlockButton({ format, icon }) {
  const editor = useSlate()
  return (
    <Button
      active={isBlockActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault()
        toggleBlock(editor, format)
      }}
    >
      <Icon>{icon}</Icon>
    </Button>
  )
}
