/** @jsxImportSource @emotion/react */

import { forwardRef } from 'react'
import { css } from '@emotion/react'

const Icon = forwardRef(
  (
    { className, ...props },
    ref
  ) => (
    <span
      {...props}
      ref={ref}
      css={css`
        font-size: 18px;
        vertical-align: text-bottom;
      `}
      className="material-icons"
    />
  )
)

export default Icon
