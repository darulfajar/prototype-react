/** @jsxImportSource @emotion/react */

import { forwardRef } from 'react'
import { css } from '@emotion/react'
import Menu from './Menu'

export const Toolbar = forwardRef(
  (
    { className, ...props },
    ref
  ) => (
    <Menu
      {...props}
      ref={ref}
      className={className}
      css={css`
        position: relative;
        padding: 1px 18px 17px;
        margin: 0 -20px;
        border-bottom: 2px solid #eee;
        margin-bottom: 20px;
      `}
    />
  )
)

export default Toolbar
