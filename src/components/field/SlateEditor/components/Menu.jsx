/** @jsxImportSource @emotion/react */

import { forwardRef } from 'react'
import { css } from '@emotion/react'

const Menu = forwardRef(
  (
    { className, ...props },
    ref
  ) => (
    <div
      {...props}
      ref={ref}
      className={className}
      css={css`
        & > * {
          display: inline-block;
        }

        & > * + * {
          margin-left: 15px;
        }
      `}
    />
  )
)

export default Menu
