/** @jsxImportSource @emotion/react */

/* eslint-disable no-nested-ternary */

import { forwardRef } from 'react'
import { css } from '@emotion/react'

const Button = forwardRef(
  (
    {
      className,
      active,
      reversed,
      ...props
    },
    ref
  ) => (
    <span
      {...props}
      ref={ref}
      css={css`
          cursor: pointer;
          color: ${reversed
        ? active
          ? 'white'
          : '#aaa'
        : active
          ? 'black'
          : '#ccc'};
        `}
    />
  )
)

export default Button
