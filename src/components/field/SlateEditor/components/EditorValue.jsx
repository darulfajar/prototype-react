/** @jsxImportSource @emotion/react */

/* eslint-disable no-nested-ternary */

import { forwardRef } from 'react'
import { css } from '@emotion/react'

const EditorValue = forwardRef(
  (
    {
      className,
      value,
      ...props
    },
    ref
  ) => {
    const textLines = value.document.nodes
      .map((node) => node.text)
      .toArray()
      .join('\n')
    return (
      <div
        ref={ref}
        {...props}
        css={css`
          margin: 30px -20px 0;
        `}
      >
        <div
          css={css`
            font-size: 14px;
            padding: 5px 20px;
            color: #404040;
            border-top: 2px solid #eeeeee;
            background: #f8f8f8;
          `}
        >
          Slate&rsquo;s value as text
        </div>
        <div
          css={css`
            color: #404040;
            font: 12px monospace;
            white-space: pre-wrap;
            padding: 10px 20px;
            div {
              margin: 0 0 0.5em;
            }
          `}
        >
          {textLines}
        </div>
      </div>
    )
  }
)

export default EditorValue
