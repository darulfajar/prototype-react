// @refresh reset

/** @jsxImportSource @emotion/react */

/* eslint-disable no-console */
/* eslint-disable no-param-reassign */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-shadow */

import {
  useCallback, useMemo, useState, useEffect,
} from 'react'
import isHotkey from 'is-hotkey'
import {
  Editable, withReact, Slate,
} from 'slate-react'
import { createEditor } from 'slate'
import { withHistory } from 'slate-history'
import { css } from '@emotion/react'
import { toggleMark } from './button/common'
import Toolbar from './Toolbar'
import Element from './Element'
import Leaf from './Leaf'
import serialize from './serialize'
import deserialize from './deserialize'

const CSS = css`
  flex: 1;
  width: 100%;
  .editor-wrapper {
    padding: 10px;
    background: #F6F9FB;
    border-radius: 5px;
    line-height: 24px;
    font-size: 14px;
    max-height: 300px;
    overflow: auto;
    border: 1px solid rgba(0, 0, 0, .1);
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    p {
      margin: 0;
    }
    ul, ol {
      margin: 0;
    }
  }
  .toolbar {
    margin: 0;
    padding: 0;
    border: 1px solid rgba(0, 0, 0, .1);
    border-bottom: 0;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    padding: 10px;
  }
`

const HOTKEYS = {
  'mod+b': 'bold',
  'mod+i': 'italic',
  'mod+u': 'underline',
  'mod+`': 'code',
}

const RichTextEditor = (props) => {
  const { value: iValue, onChange, className } = props
  const [value, setValue] = useState(iValue && iValue !== '' ? deserialize(iValue) : [{ type: 'paragraph', children: [{ text: '' }] }])
  const renderElement = useCallback((props) => <Element {...props} />, [])
  const renderLeaf = useCallback((props) => <Leaf {...props} />, [])
  const editor = useMemo(() => withHistory(withReact(createEditor())), [])

  const handleEditorChange = (value) => {
    setValue(value)
  }

  useEffect(() => {
    if (iValue !== value) onChange(serialize(value))
  }, [value, onChange, iValue])

  return (
    <div className={className} css={CSS}>
      <Slate editor={editor} value={value} onChange={handleEditorChange}>
        <Toolbar className="toolbar" />
        <div className="editor-wrapper">
          <Editable
            renderElement={renderElement}
            renderLeaf={renderLeaf}
            placeholder="Enter some rich text…"
            autoFocus
            spellCheck="false"
            onKeyDown={(event) => {
              Object.entries(HOTKEYS).forEach(([hotkey, mark]) => {
                if (isHotkey(hotkey, event)) {
                  event.preventDefault()
                  toggleMark(editor, mark)
                }
              })
            }}
          />
        </div>
      </Slate>
    </div>
  )
}

export default RichTextEditor
