/** @jsxImportSource @emotion/react */

/* eslint-disable no-console */

import { Controller } from 'react-hook-form'
import RichEditor from './RichEditor'

function SlateEditor(props) {
  const { control, name } = props
  return (
    <Controller
      control={control}
      name={name}
      render={
        ({ onChange: handleChange, value }) => (
          <RichEditor onChange={handleChange} value={value} />
        )
      }
    />
  )
}

export default SlateEditor
