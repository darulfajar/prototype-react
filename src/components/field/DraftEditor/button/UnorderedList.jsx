/** @jsxImportSource @emotion/react */

/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
import { RichUtils } from 'draft-js'
// import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import MuiIconButton from '@material-ui/core/IconButton'
import { css } from '@emotion/react'
import ulSVG from './svg/ul.svg'

function Underline(props) {
  const {
    expanded,
    onExpandEvent,
    onChange,
    currentState,
    editorState,
    className,
  } = props
  const toggleUnderline = () => {
    const newState = RichUtils.toggleBlockType(
      editorState,
      'unordered-list-item'
    )
    if (newState) {
      onChange(newState)
    }
  }
  return (
    <MuiIconButton
      onClick={toggleUnderline}
      type="button"
      size="small"
      css={css`
        border: 1px solid rgba(0,0,0,.1);
        background: rgba(0, 0, 0, .1);
        width: 25px;
        height: 25px;
        &.MuiIconButton-root {
          border-radius: 5px;
        }
        .MuiTouchRipple-root  {
          border-radius: 5px;
          top: -1px;
          left: -1px;
          right: -1px;
          bottom: -1px;
          overflow: hidden;
        }
        & .MuiTouchRipple-child {
          border-radius: 5px !important;
        }
      `}
    >
      <img
        css={css`
          height: 18px;
          width: 18px;
        `}
        src={ulSVG}
        alt=""
      />
    </MuiIconButton>
  )
}

export default Underline
