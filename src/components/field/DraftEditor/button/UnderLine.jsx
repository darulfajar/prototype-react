/** @jsxImportSource @emotion/react */

/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */

import { RichUtils } from 'draft-js'
import MuiIconButton from '@material-ui/core/IconButton'
import { css } from '@emotion/react'

function Underline(props) {
  const {
    expanded,
    onExpandEvent,
    onChange,
    currentState,
    editorState,
    className,
  } = props
  const toggleUnderline = () => {
    const newState = RichUtils.toggleInlineStyle(
      editorState,
      'UNDERLINE'
    )
    if (newState) {
      onChange(newState)
    }
  }
  return (
    <MuiIconButton
      onClick={toggleUnderline}
      type="button"
      size="small"
      css={css`
        border: 1px solid rgba(0,0,0,.1);
        background: rgba(0, 0, 0, .1);
        width: 25px;
        height: 25px;
        &.MuiIconButton-root {
          border-radius: 5px;
        }
        .MuiTouchRipple-root  {
          border-radius: 5px;
          top: -1px;
          left: -1px;
          right: -1px;
          bottom: -1px;
          overflow: hidden;
        }
        & .MuiTouchRipple-child {
          border-radius: 5px !important;
        }
      `}
    >
      <span>U</span>
    </MuiIconButton>
  )
}

export default Underline
