/** @jsxImportSource @emotion/react */

/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
import { RichUtils } from 'draft-js'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import MuiIconButton from '@material-ui/core/IconButton'
import { css } from '@emotion/react'

function Italic(props) {
  const {
    expanded,
    onExpandEvent,
    onChange,
    currentState,
    editorState,
    className,
  } = props
  const toggleItalic = () => {
    const newState = RichUtils.toggleInlineStyle(
      editorState,
      'ITALIC'
    )
    if (newState) {
      onChange(newState)
    }
  }
  return (
    <MuiIconButton
      onClick={toggleItalic}
      type="button"
      size="small"
      css={css`
        border: 1px solid rgba(0,0,0,.1);
        background: rgba(0, 0, 0, .1);
        width: 25px;
        height: 25px;
        &.MuiIconButton-root {
          border-radius: 5px;
        }
        .MuiTouchRipple-root  {
          border-radius: 5px;
          top: -1px;
          left: -1px;
          right: -1px;
          bottom: -1px;
          overflow: hidden;
        }
        & .MuiTouchRipple-child {
          border-radius: 5px !important;
        }
      `}
    >
      <span
        css={css`
          font-style: italic;
          color: #000000;
          font-size: 18px;
        `}
      >
        I
      </span>
    </MuiIconButton>
  )
}

export default Italic
