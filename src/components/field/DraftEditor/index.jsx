/* eslint-disable max-len */
/** @jsxImportSource @emotion/react */

/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
import { useState, useEffect } from 'react'
import { convertToHTML, convertFromHTML } from 'draft-convert'
import { EditorState } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
// import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { css, ClassNames } from '@emotion/react'
import { Controller } from 'react-hook-form'
import Bold from './button/Bold'
import StrikeThrough from './button/StrikeThrough'
import Italic from './button/Italic'
import UnderLine from './button/UnderLine'
import OrderedList from './button/OrderedList'
import UnorderedList from './button/UnorderedList'
import SuperScript from './button/SuperScript'

const wrapperCSS = css`
  &.rdw-editor-wrapper {
    padding: 0px;
    overflow: auto;
  }
`
const toolbarCSS = css`
  &.rdw-editor-toolbar {
    padding: 10px;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: rgba(0, 0, 0, .01);
    margin: 0;
    button {
      margin-right: 10px;
      font-size: 14px;
    }
  }
`

const editorCSS = css`
  &.rdw-editor-main {
    background: #F6F9FB;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    border: 1px solid #F1F1F1;
    border-top: none;
    padding: 10px;
  }
  & .public-DraftStyleDefault-block {
    margin: 0;
  }
`

const textEditorCSS = css`
  width: 400px;
`

const TextEditor = (props) => {
  const { defaultValue, onChange: handleChange } = props
  const [editorState, setEditorState] = useState(
    defaultValue
      ? EditorState.createWithContent(convertFromHTML(defaultValue))
      : EditorState.createEmpty()
  )

  useEffect(() => {
    const html = convertToHTML(editorState.getCurrentContent())
    if (defaultValue !== html) {
      handleChange(html)
    }
  }, [editorState, handleChange, defaultValue])

  const onEditorStateChange = (editorState) => {
    setEditorState(editorState)
  }

  return (
    <div css={textEditorCSS}>
      <ClassNames>
        {
          ({ css: className }) => (
            <Editor
              editorState={editorState}
              wrapperClassName={className`${wrapperCSS}`}
              toolbarClassName={className`${toolbarCSS}`}
              editorClassName={className`${editorCSS}`}
              onEditorStateChange={onEditorStateChange}
              toolbarCustomButtons={[
                <Bold />,
                <StrikeThrough />,
                <Italic />,
                <UnderLine />,
                <OrderedList />,
                <UnorderedList />,
                <SuperScript />,
              ]}
              toolbar={{
                options: [],
              }}
            />
          )
        }
      </ClassNames>
    </div>
  )
}

function Test(props) {
  const {
    control,
    name,
  } = props
  return (
    <Controller
      control={control}
      name={name}
      render={({ onChange: handleChange, value }) => (
        <TextEditor defaultValue={value} onChange={handleChange} />
      )}
    />
  )
}

export default Test
