/* eslint-disable no-unused-vars */
/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react'
import clsx from 'clsx'
import Button from '@material-ui/core/Button'
import PaginationItem from '@material-ui/core/PaginationItem'
import useQueryString from '@hooks/useQueryString'

const circleCSS = css`
  top: 0px;
  border: none;
  border-radius: 35px;

  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 170%;

  /* identical to box height, or 24px */
  text-align: center;

  color: #556FB5;

  &:focus {
    outline: none;
  }
  &.active {
    background: #556FB5;
    color: #FFFFFF;
  }

  &:not(:first-of-type) {
    margin-left: 7px;
  }

  &.MuiPaginationItem-page.Mui-selected.Mui-disabled {
    box-shadow: none;
    background-color: #556FB5;
    color: #FFFFFF;
    opacity: 1;
  }
  &.MuiPaginationItem-page {
    box-shadow: none;
    background-color: #FFFFFF;
    color: #556FB5;
    opacity: 1;
  }

  &.MuiPaginationItem-outlined {
    border: 1px solid rgba(85, 111, 181, .75);
  }
  &.MuiPaginationItem-root {
    padding: 0;
  }
`

function Circle(props) {
  const {
    number,
    active,
    onClick: handleClick,
  } = props
  return (

    <Button
      onClick={handleClick}
      className={clsx(active && 'active')}
      css={circleCSS}
    >
      { number }
    </Button>
  )
}

const paginationCSS = css`
  margin-top: 27px;
  display: flex;
  justify-content: center;
  align-items: center;
  & > div {
    display: flex;
  }
  & .MuiPaginationItem-icon {
    fill: #556FB5;
  }
`

function Pagination(props) {
  const { pagination } = props
  const { setPage } = useQueryString()
  return (
    pagination.pageOptions.length > 1 && (
    <div css={paginationCSS}>
      <div>
        <PaginationItem
          disabled={!pagination.canPreviousPage}
          onClick={() => {
            pagination.gotoPage(pagination.pageIndex - 1)
            setPage(pagination.pageIndex - 2)
          }}
          type="previous"
        />
        {
          [...Array(pagination.pageOptions.length)]
            .map((...[, key]) => key + 1)
            .map((number) => (
              <PaginationItem
                css={circleCSS}
                selected={(pagination.pageIndex + 1) === number}
                disabled={(pagination.pageIndex + 1) === number}
                key={number}
                variant="outlined"
                onClick={() => {
                  pagination.gotoPage(number - 1)
                  setPage(number > 1 ? number : undefined)
                }}
                page={number}
              />
            ))
        }
        <PaginationItem
          type="next"
          disabled={!pagination.canNextPage}
          onClick={() => {
            pagination.gotoPage(pagination.pageIndex + 1)
            setPage(pagination.pageIndex + 2)
          }}
        />
      </div>
    </div>
    )
  )
}

export default Pagination
