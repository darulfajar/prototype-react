/** @jsxImportSource @emotion/react */

import { css } from '@emotion/react'
import Button from '@material-ui/core/Button'

const resetButtonCSS = css`
  &.MuiButton-root {
    border-radius: 5px;
    padding: 3px 8px;
    border: 1px solid #556FB5;
    margin-right: 41px;
  }
  &.MuiButton-textPrimary {
    background-color: #FFFFFF;
  }
  .MuiButton-label {
    font-family: Poppins;
    font-style: normal;
    font-weight: normal;
    font-size: 13px;
    line-height: 22px;
    text-transform: none;
    color: #556FB5;
    width: 62px;
  }
`

function ResetFilter(props) {
  const { onClick: handleClick } = props

  return (
    <Button
      disableElevation
      css={resetButtonCSS}
      onClick={handleClick}
    >
      Reset
    </Button>
  )
}

export default ResetFilter
