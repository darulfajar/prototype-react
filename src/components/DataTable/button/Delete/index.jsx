/** @jsxImportSource @emotion/react */

import { css } from '@emotion/react'
import IconButton from '@material-ui/core/IconButton'
import deleteButtonSVG from './svg/deleteButton.svg'

const CSS = css`
  width: 30px;
  height: 30px;
`

function DeleteButton({ onClick: handleClick }) {
  return (
    <IconButton css={CSS} size="small" onClick={handleClick}>
      <img src={deleteButtonSVG} alt="" />
    </IconButton>
  )
}

export default DeleteButton
