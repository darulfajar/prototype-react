/** @jsxImportSource @emotion/react */

import { css } from '@emotion/react'
import logo from '@assets/sidebar/logo.png'

const logoCSS = css`
  width: 278px;
  height: 145px;
  background: #333333;
  background: linear-gradient(180deg, #333333 41.66%, #556FB5 300%);
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`

function Logo() {
  return (
    <div css={logoCSS}>
      <img src={logo} alt="" width="75" />
    </div>
  )
}

export default Logo
