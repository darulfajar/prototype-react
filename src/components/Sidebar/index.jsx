/** @jsxImportSource @emotion/react */

import { css } from '@emotion/react'
import Drawer from '@material-ui/core/Drawer'
import menus from '@lib/menus'

import Logo from './Logo'
import Menu from './Menu'

const sidebarCSS = css`
  display: flex;
  flex-direction: column;
  position: relative;
  z-index: 1100;
  box-shadow: 0 0 1px 1px rgba(0, 0, 0, .05);
  width: 278px;
`

const menuCSS = css`
  flex: 1;
  background: #FFFFFF;
`

function Sidebar() {
  return (
    <Drawer css={sidebarCSS} open variant="permanent">
      <Logo />
      <div css={menuCSS}>
        <Menu menus={menus} />
      </div>
    </Drawer>
  )
}

export default Sidebar
