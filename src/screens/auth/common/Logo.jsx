import logoSVG from '@assets/auth/logo.svg'

function Logo(props) {
  const { className } = props
  return (<img src={logoSVG} alt="" className={className} />)
}

export default Logo
