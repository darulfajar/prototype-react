/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react'

const backgroundCSS = css`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(#556fb5, #556fb5 0%, #fa5b61 100%);
  display: flex;
  justify-content: center;
  align-items: center;
`

function Background(props) {
  const { children } = props
  return (
    <div css={backgroundCSS}>
      { children }
    </div>
  )
}

export default Background
