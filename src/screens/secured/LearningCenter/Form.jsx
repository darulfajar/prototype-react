/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable arrow-body-style */
/** @jsxImportSource @emotion/react */
import { useState, useEffect } from 'react'
import Form from '@components/dialog/Form'
import { useForm } from 'react-hook-form'
import { css } from '@emotion/react'
import Text from '@components/field/Text'
import imageUpload from '@assets/field/upload-Image-image.svg'
import useValue from './useValue'

const CSS = css`
  min-width: 829px;
  border-radius: 5px;
  .MuiDialogContent-root {
    padding: 32px;
  }
  .field-wrap {
    width: 381px;
    display: flex;
    flex-direction: column;
  }
  .field-wrap > div {
    margin-bottom: 10px;
  }
  & .limit > div {
    display: flex;
    margin-top: 10px;
  }
  & .limit .order {
    display: flex;
    justify-content: space-between;
  }
`

export default function Dialog(props) {
  const {
    type, title, onSave: save, defaultValues,
  } = props

  const { value, open } = useValue(defaultValues)
  const [image2, setImage2] = useState({ preview: '', raw: '' })

  const {
    handleSubmit, register, control, reset,
  } = useForm({
    defaultValues: value,
  })

  async function blob(url) {
    const data = await fetch(url).then((r) => r.blob())
    // eslint-disable-next-line prefer-const
    let reader = new FileReader()
    reader.readAsDataURL(data)
    // eslint-disable-next-line func-names
    reader.onload = function () {
      setImage2({
        preview: value.image,
        raw: reader.result,
      })
    }
  }

  useEffect(() => {
    const options = {
      isDirty: false,
      touched: false,
      dirtyFields: false,
      errors: false,
    }
    if (open) {
      reset(value, options)
      setImage2({ preview: '', raw: '' })
      if (value.image) {
        blob(value.image)
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value, reset, open])

  const handleChange2 = (e) => {
    if (e.target.files.length) {
      // eslint-disable-next-line prefer-const
      let reader = new FileReader()
      reader.readAsDataURL(e.target.files[0])
      // eslint-disable-next-line func-names
      reader.onload = function () {
        setImage2({
          preview: URL.createObjectURL(e.target.files[0]),
          raw: reader.result,
        })
      }
    }
  }

  return (
    <Form
      title={title}
      type={type}
      onSubmit={handleSubmit((data) => {
        // eslint-disable-next-line prefer-const
        let datas = data
        datas.image = image2.raw
        datas.user_id = 1
        save(datas)
      })}
      css={CSS}
    >
      <div className="limit">
        <div>
          <div className="field-wrap">
            {value?.id && (
              <input name="id" type="hidden" value={value.id} ref={register} />
            )}
            <Text label="Judul" name="title" ref={register} />
            <Text label="Konten" name="content" ref={register} />
          </div>
          <label htmlFor="upload-button-2">
            {image2.preview ? (
              <img
                src={image2.preview}
                alt=""
                style={{ marginLeft: 25 }}
                width="350px"
              />
            ) : (
              <img
                src={imageUpload}
                alt=""
                style={{ marginLeft: 25 }}
                width="350px"
              />
            )}
          </label>
          <input
            type="file"
            id="upload-button-2"
            name="image"
            control={control}
            style={{ display: 'none' }}
            onChange={handleChange2}
          />
        </div>
      </div>
    </Form>
  )
}
