import { Fragment } from 'react'
import { Helmet } from 'react-helmet'
import PageTitle from '@components/PageTitle'
import Layout from '@layout/Account'
import Card from '@components/card/Account'
import userDataSVG from './svg/userData.svg'

function Account() {
  return (
    <Fragment>
      <Helmet>
        <title>Areon - Account</title>
      </Helmet>
      <PageTitle>Akun Saya</PageTitle>
      <Layout>
        <Card>
          <img src={userDataSVG} alt="" />
        </Card>
      </Layout>
    </Fragment>
  )
}

export default Account
