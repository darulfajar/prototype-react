/** @jsxImportSource @emotion/react */

import { css } from '@emotion/react'
import { useForm } from 'react-hook-form'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import TextField from './field/TextField'
import Save from './Save'

const CSS = css`
  width: 444px;
  height: 462px;
  background: #FFFFFF;
  border: 1px solid #ECF2F5;
  box-sizing: border-box;
  border-radius: 5px;
  margin: 30px;
  & > div {
    padding: 30px;
  }
  p {
    width: 381px;
    height: 81px;
    font-family: Poppins;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 27px;
    color: #676767;
  }
  .back-button {
    background: none;
    border: none;
    padding: 0;
    font-family: Poppins;
    font-size: 14px;
    font-style: normal;
    font-weight: 500;
    line-height: 24px;
    letter-spacing: 0em;
    text-align: left;
    color: #3630C9;
  }
  & form .field {
    margin-top: 12px;
  }
`

const text = 'Buat kata sandi baru '
              + 'minimal 8 karakter '
              + 'yang terdiri dari kombinasi huruf besar, '
              + 'huruf kecil, angka, dan simbol.'

function Form() {
  const history = useHistory()
  const { register } = useForm({
    defaultValues: {
      name: '',
      password: '',
      confirmPassword: '',
    },
  })
  const { name: username } = useSelector(({ auth: { userData: { name } } }) => ({ name }))

  return (
    <div css={CSS}>
      <div>
        <button className="back-button" type="button" onClick={() => history.goBack()}>&lt; Kembali</button>
        <p>{text}</p>
        <form>
          <input type="hidden" name="name" ref={register} value={username} />
          <TextField name="password" ref={register} />
          <TextField name="confirmPassword" ref={register} />
          <Save type="submit">save</Save>
        </form>
      </div>
    </div>
  )
}

export default Form
