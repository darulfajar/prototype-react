/** @jsxImportSource @emotion/react */

import { css } from '@emotion/react'
import { forwardRef } from 'react'
import Input from '@material-ui/core/Input'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import IconButton from '@material-ui/core/IconButton'
import lockSVG from './svg/lock.svg'
import eyeSVG from './svg/eye.svg'

const textFieldCSS = css`
  .MuiInput-underline::after,
  .MuiInput-underline::before {
    display: none;
  }
  .MuiInputBase-root {
    background: #F6F9FB;
    border-radius: 5px;
    width: 380px;
    height: 45px;
    border: 1px solid rgba(0,0,0,.02);
  }
  .MuiInputBase-input {
    font-family: Poppins;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 43px;
    letter-spacing: 0em;
    text-align: left;
    padding: 0 10px;
    height: 45px;
  }
  .MuiInputLabel-root {
    font-family: Poppins;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 170%;
    color: #676767;
  }
`
const TextField = forwardRef((props, ref) => {
  const { name, label } = props
  return (
    <FormControl className="field" css={textFieldCSS}>
      <InputLabel shrink>
        { label }
      </InputLabel>
      <Input
        inputRef={ref}
        inputProps={{ name }}
        startAdornment={(
          <InputAdornment position="start">
            <img src={lockSVG} alt="" />
          </InputAdornment>
          )}
        endAdornment={(
          <InputAdornment position="end">
            <IconButton size="small">
              <img src={eyeSVG} alt="" />
            </IconButton>
          </InputAdornment>
          )}
      />
    </FormControl>
  )
})

export default TextField
