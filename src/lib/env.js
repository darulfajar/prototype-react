export const isDev = process.env.NODE_ENV === 'development'
export const isStrictMode = process.env.REACT_APP_STRICT_MODE
export const authApi = process.env.REACT_APP_AUTH_API
export const masterApi = process.env.REACT_APP_MASTER_API
