import Axios from 'axios'
import { masterApi } from '@lib/env'
import qs from 'qs'
import { setBearerToken, handleUnauthorized, normalizeResponse } from './interceptor'

const api = Axios.create({
  baseURL: `${masterApi}/sentra_belajars`,
  paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'brackets' }),
})
api.defaults.headers.common.Accept = 'application/json'
api.interceptors.request.use(setBearerToken)
api.interceptors.response.use(normalizeResponse, handleUnauthorized)

export const listLearnings = (params) => api.get('/', { params })

export const createLearning = ({ learning_details, ...data }) => api.post('/create', { ...data, learning_details_attributes: learning_details })

export const updateLearning = ({ id, ...restData }) => {
  const { learning_details, ...data } = restData
  api.patch(`/${id}`, { ...data, learning_details_attributes: learning_details })
}

export const showLearning = (id) => api.get(`/${id}`)

export const deleteLearning = (id) => api.delete(`/${id}`)
