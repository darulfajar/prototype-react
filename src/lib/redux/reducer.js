import { combineReducers } from 'redux'

import ui from './slice/ui'
import auth from './slice/auth'
import notification from './slice/notification'

const rootReducer = combineReducers({ ui, auth, notification })

export default rootReducer
