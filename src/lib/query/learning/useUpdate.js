/* eslint-disable consistent-return */
/* eslint-disable camelcase */

import { updateLearning } from '@lib/axios/learning'
import { useMutation, useQueryClient } from 'react-query'
import useSuccess from '@hooks/useSuccess'
import useCloseDialog from '@hooks/useCloseDialog'

const useUpdate = () => {
  const showSuccess = useSuccess('Sukses', 'Sukses Mengubah Sentra Belajar')
  const closeDialog = useCloseDialog()
  const queryClient = useQueryClient()
  const mutation = useMutation(
    (data) => updateLearning(data)
  )

  return (data) => mutation.mutate(data, {
    onError: () => closeDialog(),
    onSuccess: () => {
      showSuccess()
      queryClient.invalidateQueries('areon-learning')
    },
  })
}

export default useUpdate
