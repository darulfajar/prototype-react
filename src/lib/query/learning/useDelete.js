import { deleteLearning } from '@lib/axios/learning'
import { useMutation, useQueryClient } from 'react-query'

const useDelete = () => {
  const queryClient = useQueryClient()
  return useMutation(
    (id) => deleteLearning(id),
    {
      onSuccess: () => {
        queryClient.invalidateQueries('areon-learning')
      },
    }
  )
}

export default useDelete
