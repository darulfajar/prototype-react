import { useQuery } from 'react-query'
import { listLearnings } from '@lib/axios/learning'
import useQueryString from '@hooks/useQueryString'
import { useMemo } from 'react'

function useIndex() {
  const { params } = useQueryString()
  const { status, data } = useQuery(
    ['areon-learning', params],
    () => listLearnings(params),
    {
      retry: false,
      staleTime: Infinity,
      keepPreviousData: true,
    }
  )
  const tableData = useMemo(
    () => ((status === 'success' && Array.isArray(data?.data?.result?.data)) ? data?.data?.result?.data : []),
    [status, data?.data?.result?.data]
  )

  const emptyPagination = useMemo(() => ({
    currentPage: 1,
    nextPage: false,
    totalItems: null,
    totalPages: null,
  }), [])

  const tablePagination = useMemo(
    () => ((status === 'success' && data?.data?.result) ? data?.data?.result : emptyPagination),
    [status, data?.data?.result, emptyPagination]
  )

  const columns = [
    ['id', 'ID', true],
    ['title', 'Judul', false],
    ['content', 'Konten', false],
    ['image', 'image', true],
    ['createdAt', 'createdAt ', true],
    ['updatedAt', 'updatedAt', true],
    ['deletedAt', 'deletedAt', true],
  ]

  const tableColumns = columns
    .map(([column, title]) => ([column, title]))
    .map(([accessor, Header]) => ({ accessor, Header }))

  const hiddenColumns = columns
    .filter(([, , hidden]) => hidden)
    .map(([column]) => column)

  return {
    tableData,
    tableColumns,
    hiddenColumns,
    tablePagination,
  }
}

export default useIndex
