/* eslint-disable camelcase */

import { createLearning } from '@lib/axios/learning'
import { useMutation, useQueryClient } from 'react-query'
import useSuccess from '@hooks/useSuccess'
import useCloseDialog from '@hooks/useCloseDialog'

const useCreateAdmin = () => {
  const showSuccess = useSuccess('Sukses', 'Sukses Menambah Sentra Belajar')
  const closeDialog = useCloseDialog()
  const queryClient = useQueryClient()
  const mutation = useMutation(
    (data) => createLearning(data)
  )

  return (data) => mutation.mutate(data, {
    onError: () => closeDialog(),
    onSuccess: () => {
      showSuccess()
      queryClient.invalidateQueries('areon-learning')
    },
  })
}

export default useCreateAdmin
