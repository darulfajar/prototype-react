import packageSVG from '@assets/menu/package.svg'
import packageActiveSVG from '@assets/menu/active/package.svg'

import { securedRoutePath } from './constant'

const rawMenu = [
  ['Sentra Belajar', [packageSVG, packageActiveSVG], `${securedRoutePath}/learning-center`],
]

const activeMenu = [
  'Sentra Belajar',
]

const menus = rawMenu.filter(
  ([title]) => activeMenu.includes(title)
)

export default [
  ...menus,
]
