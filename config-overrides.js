// eslint-disable-next-line import/no-extraneous-dependencies
const { alias } = require('react-app-rewire-alias')

module.exports = function override(config) {
  return alias({
    '@lib': 'src/lib',
    '@components': 'src/components',
    '@screens': 'src/screens',
    '@routes': 'src/routes',
    '@layout': 'src/layout',
    '@assets': 'src/assets',
    '@hooks': 'src/hooks',
  })(config)
}
